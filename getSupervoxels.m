function svFilename = getSupervoxels(pathname)

global stackGlobal;
global svStructGlobal;

D = dir([pathname '*.svb']);
svFilename = cell(length(D),1);
for ii = 1:length(D)
    svFilename{ii} = [pathname D(ii).name];
end

addpath([fileparts( mfilename('fullpath') ) filesep 'readTGMM_XMLoutput' filesep 'readTGMM_XMLoutput'])
svStructGlobal = cell( length(svFilename),1 );
for kk = 1: length(svFilename)
    [svStructGlobal{kk}, sizeIm] = readListSupervoxelsFromBinaryFile( svFilename{kk} );
end
rmpath([fileparts( mfilename('fullpath') ) filesep 'readTGMM_XMLoutput' filesep 'readTGMM_XMLoutput'])

if( isempty(sizeIm) == false )
    if( norm(sizeIm - size ( stackGlobal{ (length(stackGlobal) +1 )/2 } ) ) ~= 0 )
        sizeIm
        stackGlobal
        error 'Dimensions do not match'
    end
else
    warning 'Supervoxels binary files not found. You cannot display them'
end